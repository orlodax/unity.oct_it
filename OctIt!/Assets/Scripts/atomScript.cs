﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class atomScript : MonoBehaviour {

    public Rigidbody rb;
    public Transform nucleusSize;
    private Renderer renderAtom;

    public int atomicNumber;
    public string Symbol;
    public string Name;
    public float Mass;
    public float electroNegativity;
    public float Radius;
    public string hexColor;
    

    public static int quale;

    private float resizeRadius = 0.01f;
    private float resizeMass = 0.01f;
   
    

    void SetAtom () {
        //loads elements data from file.txt-----------------------------------------------------------------------------------------------
        string[] tabElementi = System.IO.File.ReadAllLines(@"C:\Users\orlod\Documents\unity.oct_it\OctIt!\Assets\tabel.txt");

        char[] separator = new char[] { ',', ' ' };

        string[] elSelezionato = tabElementi[quale].Split(separator);

        atomicNumber = int.Parse(elSelezionato[0]);
        Symbol  = elSelezionato[1];
        Name = elSelezionato[2];
        Mass = float.Parse(elSelezionato[3], CultureInfo.InvariantCulture.NumberFormat)*resizeMass;
        electroNegativity = float.Parse(elSelezionato[4]);
        Radius = System.Convert.ToInt16(elSelezionato[5])*resizeRadius;
        hexColor = elSelezionato[6];
        //-------------------------------------------------------------------------------------------------------------------------------

        //assigns masses-------------------
        rb = GetComponent<Rigidbody>();
        rb.mass = Mass;
        
        //assigns dimensions to the hull-------------------------------
        Transform size = GetComponent<Transform>();
        size.localScale = new Vector3(Radius, Radius, Radius);
        //and to the nucleus
        nucleusSize = transform.Find("Nucleus");
        nucleusSize.localScale = size.localScale / 10;

        //assigns colors-----------------------------------------
        renderAtom = GetComponent<Renderer>();
        Color newCol;
        ColorUtility.TryParseHtmlString(hexColor, out newCol);
        renderAtom.material.color = newCol;
  
    }

    // Use this for initialization
    void Start () {

        SetAtom();

    }

  	// Update is called once per frame
	void Update () {
		
	}

    public float speed;

    private void FixedUpdate(){
        //  if (==true)
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

    }

   
}
