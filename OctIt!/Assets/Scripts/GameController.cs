﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameController : MonoBehaviour {

    public GameObject Atom;
    public Vector3 spawnValues;
    public int numberOfAtoms;

    public int biggestAtom = 30;

    // Use this for initialization
    void Start () {

        StartCoroutine (SpawnAtoms());

    }

    IEnumerator SpawnAtoms() {

        int i = 0;
        while (i<numberOfAtoms)
        {
            atomScript.quale = Random.Range(1, biggestAtom);
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, Random.Range(-spawnValues.z, spawnValues.z));
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(Atom, spawnPosition, spawnRotation);
            i++;
            yield return new WaitForSeconds(0.1f);
        }
        
    }
	
	// Update is called once per frame
	void Update () {
            
       

    }
}
